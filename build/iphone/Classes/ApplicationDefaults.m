/**
* Appcelerator Titanium Mobile
* This is generated code. Do not modify. Your changes *will* be lost.
* Generated code is Copyright (c) 2009-2011 by Appcelerator, Inc.
* All Rights Reserved.
*/
#import <Foundation/Foundation.h>
#import "TiUtils.h"
#import "ApplicationDefaults.h"
 
@implementation ApplicationDefaults
  
+ (NSMutableDictionary*) copyDefaults
{
    NSMutableDictionary * _property = [[NSMutableDictionary alloc] init];

    [_property setObject:[TiUtils stringValue:@"JFQqQCQT5pc3ty8AfSLvYQi7O4NmWrUu"] forKey:@"acs-oauth-secret-production"];
    [_property setObject:[TiUtils stringValue:@"rWAK9hqpiR8nhxMAwqIkhcwti0iA0Sj5"] forKey:@"acs-oauth-key-production"];
    [_property setObject:[TiUtils stringValue:@"YsfyEnTjL38MMSMFsO5xyWIcvqpeLwYt"] forKey:@"acs-api-key-production"];
    [_property setObject:[TiUtils stringValue:@"ay7qXc2oBY25Oz757VDqeAjjYhNDndGR"] forKey:@"acs-oauth-secret-development"];
    [_property setObject:[TiUtils stringValue:@"TOA3e1a5WWytwyTxJaaBJdXFr9PjlC72"] forKey:@"acs-oauth-key-development"];
    [_property setObject:[TiUtils stringValue:@"57bOgpjMAGRFT5L0szjhKu6Xx3SiwTEw"] forKey:@"acs-api-key-development"];
    [_property setObject:[TiUtils stringValue:@"system"] forKey:@"ti.ui.defaultunit"];

    return _property;
}
@end
