function LanguageWindow(){
	var ApplicationTabGroup = require('ui/iphone/ApplicationTabGroup');
	var self = Ti.UI.createWindow({
		backgroundImage:'idioma_fondo-02.png',	
		orientationModes:[Titanium.UI.PORTRAIT]
	});
	
	var title=Ti.UI.createImageView({
		image:'/idioma_logo.png',
		top: 10	
	});
	
	var esButton = Ti.UI.createButton({
		backgroundImage: 'idioma_boton_espanol_off.png',
		backgroundFocusedImage: 'idioma_boton_espanol_on.png',
		backgroundSelectedImage: 'idioma_boton_espanol_on.png',
		bottom:50,
		width:150,	
		height:80
	});
	
	var enButton = Ti.UI.createButton({
		backgroundImage: 'idioma_boton_english_off.png',
		backgroundFocusedImage: 'idioma_boton_english_on.png',
		backgroundSelectedImage: 'idioma_boton_english_on.png',
		bottom:150,
		width:150,
		height:80
	});
	
	enButton.addEventListener('click',function(e) { 
		Ti.API.info("boton clickeado ingles");
		Ti.App.Properties.setString('language', 'en');	
		var applicationTabGroup = new ApplicationTabGroup();	
		applicationTabGroup.open();		
	}); 
	
	esButton.addEventListener('click',function(e) {
		Ti.API.info("boton clickeado espanol");
		Ti.App.Properties.setString('language', 'es');  
	    var applicationTabGroup = new ApplicationTabGroup();
	    applicationTabGroup.open();
	    
	}); 
		
	self.add(title); 
	self.add(enButton);	 
	self.add(esButton);
	
	return self;
}
module.exports = LanguageWindow;
