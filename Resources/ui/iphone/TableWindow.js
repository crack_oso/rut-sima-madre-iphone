function TableWindow() {
	Ti.App.Properties.setString('windowSelected', 'ui/iphone/TableWindow2.js');
	var http = require('helpers/http');
	var j;
	// Ventana donde estara la tabla
	var tableWindow = Ti.UI.createWindow({
		title:'Tu Ruta',
		barColor : '#111',
		backgroundColor : 'white' /*'#bfb7aa'*/,
		orientationModes:[Titanium.UI.PORTRAIT]
	});
	//Ventana de detalle de cada punto
	var DetailPointWindow = require('ui/iphone/DetailPointWindow');
	var move = false;
	var routesData;
	var goMap = Ti.UI.createButton({
		title : 'Ir al mapa'
	});
	var changeButton = Ti.UI.createButton({
		title : 'Cambiar',
	});

	var data = [];
	var tableView = Ti.UI.createTableView({
		width : 310,
		top : 0,
		left : 5,
		right : 5,
		height : 367,
		backgroundColor :'transparent',
	});

	var rows = function(routes) {
		routesData = routes;
		j = routes.length;
		for (var i = 0; i < j ; i++) {
			if (i % 2 == 0) {
				var row = Ti.UI.createTableViewRow({
					height : 80,
					posicion : routes[i].posicion,
					backgroundColor : '#e9e5da',
				});
			} else {
				var row = Ti.UI.createTableViewRow({
					height : 80,
					posicion : routes[i].posicion,
					backgroundColor : '#bfb7aa',
				});
			}
			var image = Ti.UI.createImageView({
				width : 75,
				height : 75,
				left : 5,
				image : 'http://publicidadenlinea.com/rutisimamadre/web/uploads/ubicaciones/' + routes[i].archivo1,
			});

			var name = Ti.UI.createLabel({
				text : routes[i].titulo,
				font : {
					fontSize : 18,
					fontWeight : 'bold'
				},
				height:20,
				left : 85,
				top : 10,
				align : Titanium.UI.TEXT_ALIGNMENT_LEFT,
				color : 'black',
				touchEnabled : false
			});
			var descripcion = Ti.UI.createLabel({
				text : routes[i].descripcion,
				font : {
					fontSize : 14
				},
				left : 85,
				right : 5,
				height: 40, 
				top : 30,
				align : Ti.UI.TEXT_ALIGNMENT_LEFT,
				color : 'black',
				touchEnabled : false
			});

			row.add(image);
			row.add(name);
			row.add(descripcion);
			data.push(row)


		};		
		tableView.setData(data);
		//Ti.API.info(tableView.data[0].rows.length);
		tableView.addEventListener('move',function(e){
			var routesData = JSON.parse(Ti.App.Properties.getString('routes'));
			for(conta=0;conta<routesData.length;conta++){
				if (conta % 2 == 0) {
					tableView.data[0].rows[conta].backgroundColor = '#e9e5da'
				} else {
					tableView.data[0].rows[conta].backgroundColor = '#bfb7aa'
				}
			}
		})
		
		/*
 * Funciones para generar la recarga por medio del scroll
 */
		function formatDate(){
			var date = new Date();
			var datestr = date.getMonth()+'/'+date.getDate()+'/'+date.getFullYear();
			if (date.getHours()>=12){
				datestr+=' '+(date.getHours()==12 ? date.getHours() : date.getHours()-12)+':'+date.getMinutes()+' PM';
			}else{
				datestr+=' '+date.getHours()+':'+date.getMinutes()+' AM';
			}
			return datestr;
		}
		var border = Ti.UI.createView({
			backgroundColor:"black",
			height:2,
			bottom:0
		});
		var tableHeader = Ti.UI.createView({
			backgroundColor:"#bfb7aa",
			width:320,
			height:60
		});
		tableHeader.add(border);

		var arrow = Ti.UI.createView({
			backgroundImage:"images/whiteArrow.png",
			width:23,
			height:60,
			bottom:10,
			left:20
		});
		
		var statusLabel = Ti.UI.createLabel({
			text:"Deslice para actualizar",
			left:55,
			width:200,
			bottom:45,
			height:"auto",
			color:"black",
			textAlign:"center",
			font:{fontSize:13,fontWeight:"bold"},
			shadowColor:"#999",
			shadowOffset:{x:0,y:1}
		});
		
		var lastUpdatedLabel = Ti.UI.createLabel({
			text:"Ultima actualizacion: "+formatDate(),
			left:55,
			width:200,
			bottom:15,
			height:"auto",
			color:"black",
			textAlign:"center",
			font:{fontSize:12},
			shadowColor:"#999",
			shadowOffset:{x:0,y:1}
		});
		
		var actInd = Titanium.UI.createActivityIndicator({
			left:20,
			bottom:13,
			width:30,
			height:30
		});
		
		tableHeader.add(arrow);
		tableHeader.add(statusLabel);
		tableHeader.add(lastUpdatedLabel);
		tableHeader.add(actInd);
		
		tableView.headerPullView = tableHeader;
		
		var pulling = false;
		var reloading = false;
		
		function beginReloading(){
			setTimeout(endReloading,3000);
		}
		
		function endReloading(){
			new http.getPoints({
				onload : function(response) {
					for (var i = 0; i < response.length; i++) {
						response[i].posicion = i + 1;
						//Ti.API.info(JSON.stringify(response[i]));
					}
					Ti.App.Properties.setString('routes', JSON.stringify(response));
					Ti.App.fireEvent('NewPins', {
						data : response
					});
					rows(response);
				},
				onerror : function() {
					//alert('Tiene conexión a internet?');
				}
			});
			
			if(tableView.data[0]){
				var numRows = tableView.data[0].rowsCount;
				for(s=0;s<numRows;s++){
					tableView.deleteRow(s);
				}
			}
			
			// when you're done, just reset
			tableView.setContentInsets({top:0},{animated:true});
			reloading = false;
			var lang = Ti.App.Properties.getString('language');
			if (lang == 'es'){
				lastUpdatedLabel.text = "Ultima Actualizacion: "+formatDate();
				statusLabel.text = "Desliza para actualizar";
			} else if (lang == 'en'){
				lastUpdatedLabel.text = "Last Update: "+formatDate();
				statusLabel.text = "Slide to Refresh";
			}
			actInd.hide();
			arrow.show();
		}

		tableView.addEventListener('scroll',function(e){
			var offset = e.contentOffset.y;
			if (offset < -65.0 && !pulling && !reloading){
				var t = Ti.UI.create2DMatrix();
				t = t.rotate(-180);
				pulling = true;
				arrow.animate({transform:t,duration:180});
				var lang = Ti.App.Properties.getString('language');
				if (lang == 'es'){
					statusLabel.text = "Suelta para Actualizar";
				} else if (lang == 'en'){
					statusLabel.text = "Loose to Refresh";
				}
			}else if((offset > -65.0 && offset < 0 ) && pulling && !reloading){
				pulling = false;
				var t = Ti.UI.create2DMatrix();
				arrow.animate({transform:t,duration:180});
				var lang = Ti.App.Properties.getString('language');
				if (lang == 'es'){
					statusLabel.text = "Desliza para actualizar";
				} else if (lang == 'en'){
					statusLabel.text = "Slide to Refresh";
				}
			}    
		});
		
		tableView.addEventListener('dragEnd', function(){	
			if(pulling && !reloading){
				reloading = true;
				pulling = false;
				arrow.hide();
				actInd.show();
				var lang = Ti.App.Properties.getString('language');
				if (lang == 'es'){
					statusLabel.text = "Actualizando...";
				} else if (lang == 'en'){
					statusLabel.text = "Updating...";
				}
				tableView.setContentInsets({top:60},{animated:true});
				tableView.scrollToTop(-60,true);
				arrow.transform=Ti.UI.create2DMatrix();
				beginReloading();
			}
		});
		
	Ti.App.addEventListener('espanolTable',function(e){
		tableWindow.setTitle('Tu Ruta');
		goMap.setTitle('Ir al mapa');
		changeButton.setTitle('Cambiar');
		statusLabel.setText('Desliza para Actualizar');
		lastUpdatedLabel.setText("Ultima actualizacion: "+formatDate());
	})
	Ti.App.addEventListener('englishTable',function(e){
		tableWindow.setTitle('Route');
		goMap.setTitle('Go to Map');
		changeButton.setTitle('Change');
		statusLabel.setText('Slide to Refresh');
		lastUpdatedLabel.setText("Last Update: "+formatDate());
	})

/*
 * Listener para actualizar los colores de los rows
 */
		
		data = [];
	}

	tableWindow.leftNavButton = goMap;
	tableWindow.rightNavButton = changeButton;
	tableWindow.add(tableView);
	
	goMap.addEventListener('click', function(e) {
		Ti.App.fireEvent('trazaRuta',{name:'bar'});
		Ti.App.fireEvent('goMap',{name:'bar'});
	});

	changeButton.addEventListener('click', function() {
		if (move) {
			tableView.moving = false;
			var lang = Ti.App.Properties.getString('language');
			if (lang == 'es'){
				changeButton.title = 'Cambiar';
			} else if (lang == 'en'){
				changeButton.title = 'Change';
			}
			move = false;

			var newOrderData = [];
			for (var i = 0; i < tableView.data[0].rows.length; i++) {
				//Ti.API.info('#: ' + i + 'Posicion: ' + tableView.data[0].rows[i].posicion);

				for (var l = 0; l < routesData.length; l++) {
					if (routesData[l]['posicion'] === tableView.data[0].rows[i].posicion) {
						newOrderData.push(routesData[l]);
						break;
					}
				}
			}
			Ti.App.Properties.setString('routes', JSON.stringify(newOrderData));
			//Ti.API.info(newOrderData);

		} else {
			tableView.moving = true;
			changeButton.title = 'Ok';
			move = true;
		}
	});

	tableView.addEventListener('click', function(e) {
		var detailPointWindow = new DetailPointWindow(e.rowData.posicion, j);
		detailPointWindow.open();
	});

	tableWindow.addEventListener('open', function() {
		routesData = JSON.parse(Ti.App.Properties.getString('routes'));
		if (routesData) {
			rows(routesData);
		}
	});
	
	return tableWindow;
}

module.exports = TableWindow;
