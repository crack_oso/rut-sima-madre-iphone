function Distancia(latitud1, longitud1, latitud2, longitud2) {

	function RadToDeg(radians) {
		return radians * (180 / Math.PI);
	};

	function DegToRad(degrees) {
		return degrees * (Math.PI / 180);
	};

	function Bearing(lat1, long1, lat2, long2) {
		// alert(lat1+','+long1+','+lat2+','+long2);
		//Convert input values to radians
		lat1 = DegToRad(lat1);
		long1 = DegToRad(long1);
		lat2 = DegToRad(lat2);
		long2 = DegToRad(long2);
		// alert(lat1+','+long1+','+lat2+','+long2);
		var deltaLong = long2 - long1;

		var y = Math.sin(deltaLong) * Math.cos(lat2);
		var x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1) * Math.cos(lat2) * Math.cos(deltaLong);
		var bearing = Math.atan2(y, x);
		return ConvertToBearing(RadToDeg(bearing));
	};

	function ConvertToBearing(deg) {
		return (deg + 360) % 360;
	};

	function Distance(lat1, lon1, lat2, lon2) {
		var R = 6371;
		// km

		var dLat = DegToRad(lat2 - lat1);
		var dLon = DegToRad(lon2 - lon1);
		var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(DegToRad(lat1)) * Math.cos(DegToRad(lat2)) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
		var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		var d = R * c;

		Ti.API.debug("km " + d);
		Ti.API.debug("miles " + d / 1.609344);
		
		if (d * 1000 < 1000) {
			return Math.round(d * 1000) + ' meters'
		} else {
			return Math.round(d) + ' kms';
		}
	};

	var mexico = Distance(latitud1, longitud1, latitud2, longitud2);
	
	return mexico;

}
