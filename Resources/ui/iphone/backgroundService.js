Ti.API.info("servicio activado!");
var alerta= function(){
	var notificaciones=Titanium.App.Properties.getString('Notificaciones');
	Ti.API.info(notificaciones);
	var userLatitud;
	var userLongitud;
	Ti.Geolocation.getCurrentPosition(function(e){
		userLatitud = e.coords.latitude;
		userLongitud = e.coords.longitude;
		function distance(lat1, lon1, lat2, lon2, unit) {
			var radlat1 = Math.PI * lat1/180;
			var radlat2 = Math.PI * lat2/180;
			var radlon1 = Math.PI * lon1/180;
			var radlon2 = Math.PI * lon2/180;
			var theta = lon1-lon2;
			var radtheta = Math.PI * theta/180;
			var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
			dist = Math.acos(dist);
			dist = dist * 180/Math.PI;
			dist = dist * 60 * 1.1515;
			if (unit=="K") { dist = dist * 1.609344; }
				if (unit=="N") { dist = dist * 0.8684; }
				return dist;
		}
	var points = JSON.parse(Ti.App.Properties.getString('routes'));
		for(i=0;i<points.length;i++){
			var resultado = distance(userLatitud,userLongitud,points[i].latitud,points[i].longitud,'K');
			//Ti.API.info(resultado);
			///////// la distancia actual entre 2 puntos es de .5 kilometros //////////
			if (resultado<0.5 && notificaciones=='Activado'){
				notification = Ti.App.iOS.scheduleLocalNotification({
					alertBody:'Cerca del Punto: ' + points[i].titulo,
					alertAction:"OK",
					date:new Date(new Date().getTime() + 10)
				});
			}
		}
	});
}

/////////// Intervalo de tiempo de alerta //////////
var timer = setInterval(alerta, 10000);