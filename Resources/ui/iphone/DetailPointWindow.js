function DetailPointWindow(id, count) {
	Ti.API.info(id);
	var pinActivo = false;
	var activarFotos = false;
	var detailWindow = Ti.UI.createWindow({
		barColor : '#111',
		heigth:44+367,
		top:0,
		orientationModes:[Titanium.UI.PORTRAIT]
		//backgroundColor : 'white',
	});

	var urlMusic = '';
	var active = false;
	var weburl = '';

	var scrollView = Ti.UI.createScrollView({
		width : '100%',
		height : 44+367,
		top : 0,
		left : 0,
		layout : 'vertical',
		backgroundColor : 'white'
	});

	var routes = JSON.parse(Ti.App.Properties.getString('routes'));
	//Ti.API.info(routes);
	var data = null;
	
// Elementos de la cabecera, flechas izq y der y los puntos 	
	var contenedor0 = Ti.UI.createView({
		top : 0,
		width: 320,
		right: 0,
		backgroundColor :'black',
		height : 50
	});
	var contenedor1 = Ti.UI.createView({
		top : 0,
		//height:'100%',
		right: 5,
		width: 310,
		backgroundColor :'#bfb7aa',
		height : Ti.UI.SIZE
	});
	var fondoTop = Ti.UI.createImageView({
		image : 'barra_inferior_fondo-27.png',
		top : 0,
		width : '320dp',
		height : '50dp',
		backgroundColor : 'transparent'
	});

	var izquierdaButton = Ti.UI.createImageView({
		image : 'flecha_off-36.png',
		top : '3dp',
		width : '30dp',
		left : '0dp',
		backgroundColor : 'transparent'
	});
	var derechaButton = Ti.UI.createImageView({
		//title:'derecha',
		image : 'flecha_off-35.png',
		top : '3dp',
		width : '30dp',
		right : '0dp',
		backgroundColor : 'transparent'
	});
	
	
//Elementos de Titulo del punto	
	var imagenTitulo = Ti.UI.createImageView({
		width : 70,
		height : 70,
		top : 5,
		left : 20,
		backgroundColor:'#bfb7aa'
	});
	var imagenesArray=[];
	
	var contenedorFotos = Ti.UI.createView({
		top : 0,
		//height:'100%',
		right: 5,
		width: 310,
		backgroundColor :'white',
		height : 295
	});
	
	var imagesCatalog = [];
	
	var imagenTitulo2 = Ti.UI.createImageView({
		width : 280,
		height : 200,
		top : 5,
		left : 15,
		backgroundColor: 'white'
	});
	
	var imagenTitulo3 = Ti.UI.createImageView({
		width : 80,
		height : 80,
		top : 210,
		left : 15,
		backgroundColor: 'white'
	});
	var imagenTitulo4 = Ti.UI.createImageView({
		width : 80,
		height : 80,
		top : 210,
		//left : 15,
		backgroundColor: 'white'
	});
	var imagenTitulo5 = Ti.UI.createImageView({
		width : 80,
		height : 80,
		top : 210,
		right : 15,
		backgroundColor: 'white'
	});

	var titleRoute = Ti.UI.createLabel({
		top : 10,
		left : 100,
		width : 200,
		height : 20,
		font : {
			fontFamily : 'Verdana',
			fontSize : '16px',
			fontWeight : 'bold'
		}
	});
	var fechaRoute = Ti.UI.createLabel({
		top : 30,
		left : 100,
		height : 35,
		width : 200,
		font : {
			fontFamily : 'Verdana',
			fontSize : '14px',
			//fontWeight : 'bold'
		}
	});
	var imageSeparacion = Ti.UI.createImageView({
		image : 'separacion.png',
		width : '290px',
		top : 80
	});
	
	var contenedor2 = Ti.UI.createView({
		top : 0,
		//height:'100%',
		right: 5,
		width: 310,
		backgroundColor :'#bfb7aa',
		height : 70
	});
	var direccionRoute = Ti.UI.createLabel({
		top : 5,
		left : 20,
		//right : '20px',
		width: 270,
		height : 45,
		font : {
			fontFamily : 'Verdana',
			fontSize : '12px',
		}
	});
	
	var contenedorDescripcion = Ti.UI.createView({
		top : 5,
		right: 5,
		width: 310,
		backgroundColor :'#bfb7aa',
		height : Ti.UI.SIZE
	});
	var descriptionRoute = Ti.UI.createLabel({
		top : 5,
		left : 20,
		width: 270,
		//right : '20px',
		font : {
			fontFamily : 'Verdana',
			fontSize : '12px',
		}
	});

	contenedor0.add(fondoTop);
	contenedor0.add(izquierdaButton);
	contenedor0.add(derechaButton);
	contenedor1.add(imagenTitulo);
	contenedor1.add(titleRoute);
	contenedor1.add(fechaRoute);
	contenedor1.add(imageSeparacion);
	contenedor2.add(direccionRoute);
	contenedorFotos.add(imagenTitulo2);
	contenedorFotos.add(imagenTitulo3);
	contenedorFotos.add(imagenTitulo4);
	contenedorFotos.add(imagenTitulo5);
	contenedorDescripcion.add(descriptionRoute);
	
// Elementos del reproductor de sonidos
	var repView = Ti.UI.createView({
		top : 10,
		width : 320,
		backgroundColor : '#545252',
		height : 80
	});

	var returnImage = Ti.UI.createButton({
		backgroundImage : 'images/audio_botones_off_43.png',
		backgroundSelectedImage:'images/audio_botones_on_43.png',
		backgroundFocusedImage: 'images/audio_botones_on_43.png',
		width : 30,
		height : 30,
		top : 10,
		left : 120
	});

	var playImage = Ti.UI.createButton({
		backgroundImage : 'images/audio_botones_off_44.png',
		backgroundSelectedImage:'images/audio_botones_on_44.png',
		backgroundFocusedImage: 'images/audio_botones_on_44.png',
		width : '30px',
		height : '30px',
		top : '10px',
		right : '120px'
	});

	var audioPlayer = Ti.Media.createSound({
		allowBackground : false
	});

	var progressBar = Titanium.UI.createProgressBar({
		width : 250,
		min : 0,
		max : 1,
		value : 0,
		color : '#fff',
		//style : Titanium.UI.iPhone.ProgressBarStyle.PLAIN,
		bottom : 25
	});

	repView.add(returnImage);
	repView.add(playImage);
	repView.add(progressBar);
	
// Aqui termina


// Imagenes para arreglo
	var Punto;
	for (var i = 0; i < routes.length; i++) {
		Ti.API.info('id: ' + routes[i]['posicion']);
		if (routes[i]['posicion'] == id) {
			Punto = routes[i]['titulo'];
			/*if(routes[i]['archivo2'] != null){imagenesArray.push(routes[i]['archivo2']);}
			if(routes[i]['archivo3'] != null){imagenesArray.push(routes[i]['archivo3']);}
			if(routes[i]['archivo4'] != null){imagenesArray.push(routes[i]['archivo4']);}
			if(routes[i]['archivo5'] != null){imagenesArray.push(routes[i]['archivo5']);}
			if(routes[i]['archivo6'] != null){imagenesArray.push(routes[i]['archivo6']);}*/
			if(routes[i]['archivo1'] != null && routes[i]['archivo1'] != ''){
				imagenTitulo.image = 'http://publicidadenlinea.com/rutisimamadre/web/uploads/ubicaciones/' + routes[i]['archivo1'];
				imagesCatalog.push('http://publicidadenlinea.com/rutisimamadre/web/uploads/ubicaciones/' + routes[i]['archivo1']);
			}
			
			if(routes[i]['archivo2'] != null && routes[i]['archivo2'] != ''){
				imagenTitulo2.image = 'http://publicidadenlinea.com/rutisimamadre/web/uploads/ubicaciones/' + routes[i]['archivo2'];
				imagesCatalog.push('http://publicidadenlinea.com/rutisimamadre/web/uploads/ubicaciones/' + routes[i]['archivo2']);
				activarFotos = true;
			}
			if(routes[i]['archivo3'] != null && routes[i]['archivo3'] != ''){
				imagenTitulo3.image = 'http://publicidadenlinea.com/rutisimamadre/web/uploads/ubicaciones/' + routes[i]['archivo3'];
				imagesCatalog.push('http://publicidadenlinea.com/rutisimamadre/web/uploads/ubicaciones/' + routes[i]['archivo3']);
				activarFotos = true;
			}
			if(routes[i]['archivo3'] != null && routes[i]['archivo3'] != ''){
				imagenTitulo4.image = 'http://publicidadenlinea.com/rutisimamadre/web/uploads/ubicaciones/' + routes[i]['archivo4'];
				imagesCatalog.push('http://publicidadenlinea.com/rutisimamadre/web/uploads/ubicaciones/' + routes[i]['archivo4']);
				activarFotos = true;
			}
			if(routes[i]['archivo3'] != null && routes[i]['archivo3'] != ''){
				imagenTitulo5.image = 'http://publicidadenlinea.com/rutisimamadre/web/uploads/ubicaciones/' + routes[i]['archivo5'];
				imagesCatalog.push('http://publicidadenlinea.com/rutisimamadre/web/uploads/ubicaciones/' + routes[i]['archivo5']);
				activarFotos = true;			
			}
			//imagenTitulo5.image = 'http://publicidadenlinea.com/rutisimamadre/web/uploads/ubicaciones/' + routes[i]['archivo5'];
			
			titleRoute.text = routes[i]['titulo'];
			fechaRoute.text = routes[i]['fecha'];
			direccionRoute.text = routes[i]['direccion'];
			descriptionRoute.text = routes[i]['descripcion'];
			Ti.API.info("estado de pin: " + routes[i].status);
			if(routes[i]['status']=='Activado'){
				pinActivo = true;
			}
			
			if (routes[i]['audio'] != '' && routes[i]['audio'] != null) {
				urlMusic = 'http://www.publicidadenlinea.com/rutisimamadre/web/uploads/ubicaciones/' + routes[i]['audio'];
				progressBar.show();
			}
			if (routes[i]['url'] != '' && routes[i]['url'] != null) {
				weburl = routes[i]['url'];
			}
			break;
		}
	}
	var scrollTab = Titanium.UI.createScrollView({
		contentWidth:50*(routes.length),
		contentHeight:40,
		top:0,
		left:35,
		height:50,
		width:250,
		borderRadius:0,
		backgroundImage:'barra_inferior_fondo-27.png'
	});
	
	contenedor0.add(scrollTab);
	imageView = [];

// Definicion de los elementos que estaran en la cabecera del detalle y sus listeners	
	for(t=0;t<=(routes.length-1);t++){
		var pos;
		if(t==0){
			pos=0;
		}else{
			pos=50*(t);
		}
				
		var newView = Ti.UI.createView({
			backgroundColor:'transparent',
			width:40,
			height:40,
			left: pos,
			id:id-1
		});
			
		scrollTab.add(newView);
		if(t==(id-1)){
			imageView[t] = Titanium.UI.createImageView({
				image:'boton_-17.png',
				id:t+1
			});	
		}else{
			imageView[t] = Titanium.UI.createImageView({
				image:'boton_-18.png',
				id:t+1
			});
		}
		newView.add(imageView[t]);	
	}
	
	for(var q=0;q<routes.length;q++){
		imageView[q].addEventListener('click',function(e){
			//Ti.API.info(e.source.id);
			detailWindow.close();
			var DetailPointWindow = require('ui/iphone/DetailPointWindow');
			var detailPointWindow = new DetailPointWindow(e.source.id, 1);
			detailPointWindow.open();	
		});	
	}
	
	izquierdaButton.addEventListener('click', function(e){
		if(id!=1){
			detailWindow.close();
			var DetailPointWindow = require('ui/iphone/DetailPointWindow');
			var detailPointWindow = new DetailPointWindow(id-1, 1);
			detailPointWindow.open();
		}
	});
	
	derechaButton.addEventListener('click', function(e){
		var lastPoint = routes.length;
		if(id!=routes.length){
			detailWindow.close();
			var DetailPointWindow = require('ui/iphone/DetailPointWindow');
			var detailPointWindow = new DetailPointWindow(id+1, 1);
			detailPointWindow.open();
		}
	});	
//Funcion para mostrar las fotos si es que se cuenta con ellas	
	var muestraFotos=function(pagina){
		var imagenes=[];
		var fotosWindow=Ti.UI.createWindow({
			title:'Galeria',
			modal:true,
			barColor:'black',
			orientationModes:[Titanium.UI.LANDSCAPE_LEFT, Titanium.UI.LANDSCAPE_RIGHT,  Titanium.UI.PORTRAIT]
		});
		var cerrar = Ti.UI.createButton({
			title:'cerrar',
			height:20,
			width:30
		});
		cerrar.addEventListener('click',function(e){
			fotosWindow.close();
		})
		fotosWindow.leftNavButton = cerrar;
		for(s=0;s<imagesCatalog.length;s++){
			var nuevaFoto =Ti.UI.createImageView({
				backgroundColor:'black',
				height:'100%',
				width:'100%',
				image:imagesCatalog[s]
			});
			imagenes.push(nuevaFoto);
		}
		var scrollableView = Titanium.UI.createScrollableView({
			views:imagenes,
			showPagingControl:false,
			pagingControlHeight:30,
			maxZoomScale:1.5,
			currentPage:pagina
		});
		fotosWindow.add(scrollableView);
		fotosWindow.open();
	}
	
	Ti.API.info(imagesCatalog);
	imagenTitulo.addEventListener('click',function(e){
		muestraFotos(0);
	})
	imagenTitulo2.addEventListener('click',function(e){
		muestraFotos(1);
	})
	imagenTitulo3.addEventListener('click',function(e){
		muestraFotos(2);
	})
	imagenTitulo4.addEventListener('click',function(e){
		muestraFotos(3);
	})
	imagenTitulo5.addEventListener('click',function(e){
		muestraFotos(4);
	})
//Elementos de Video	
	var contenedorVideo = Ti.UI.createView({
		backgroundColor:'white',
		width:320,
		height:260,
		top:0
	});
	var youtubeImagen = Ti.UI.createImageView({
		image : 'http://img.youtube.com/vi/' + weburl + '/1.jpg',
		backgroundColor : 'transparent',
		width : '300dp',
		height : '250dp',
		top : 0//'-260px'
	});
	var youtubeVideo = Ti.UI.createImageView({
		image : 'youtube_play.png',
		backgroundColor : 'transparent',
		width : '150dp',
		height : '150dp',
		top : 50//'-200px'
	});
// Termina Elementos de Video


	Ti.App.addEventListener('NewPins', function(e) {
		if (routes != null) {
			mapView.removeRoute(routes);
		}
		//mapView.removeAllAnnotations();
		var routes = e.data;
		//pins(routes);
	});

	returnImage.addEventListener('click', function() {
		Ti.API.info('Time' + audioPlayer.getTime() + ' duration: ' + audioPlayer.getDuration());
		audioPlayer.time = audioPlayer.getTime() - 10;
		Ti.API.info('Time' + audioPlayer.getTime() + ' duration: ' + audioPlayer.getDuration());
	});

	playImage.addEventListener('click', function() {
		if (urlMusic != '' && urlMusic != null) {
			if (!active) {
				audioPlayer.url = urlMusic;
				progressBar.max = audioPlayer.duration;
				active = true;
				Ti.API.info('!Active');

				if (audioPlayer.playing || audioPlayer.paused) {
					audioPlayer.stop();
					//	        		playImage.image ='/images/audio_botones_on-44.png';
				} else {
					audioPlayer.play();
					//playImage.image ='/images/audio_botones_on-44.png';
					var i = setInterval(function() {
						if (audioPlayer.isPlaying) {
							progressBar.value = audioPlayer.time;
						}

					}, 500);
				}
			} else {
				if (audioPlayer.playing || audioPlayer.paused) {
					audioPlayer.stop();
				} else {
					audioPlayer.play();
					//playImage.image ='/images/audio_botones_on-44.png';
					var i = setInterval(function() {
						if (audioPlayer.isPlaying) {
							progressBar.value = audioPlayer.time;
						}

					}, 500);
				}
			}
		} else {
			//alert('No tiene audio');
		}

	});

//Listener para reproducir video
	youtubeVideo.addEventListener('click', function() {
		if (weburl != '') {
			var webview = Titanium.UI.createWebView({
				url:weburl
			});
			
			//weburl='P4ihWyNqG0U';
			var webview = Ti.UI.createWebView({
				url : 'http://www.youtube.com/embed/' + weburl + '?autoplay=1&autohide=1&cc_load_policy=0&color=white&controls=0&fs=0&iv_load_policy=3&modestbranding=1&rel=0&showinfo=0'
			});
			var window = Titanium.UI.createWindow({
				barColor:'black',
				title : 'Rutisima Madre'
			});
			var b = Ti.UI.createButton({
				title : 'Atrás'
			});
			b.addEventListener('click', function() {
				window.close();
			});
			window.add(webview);
			window.setLeftNavButton(b);
			window.open({
				modal : true
			});
		}
	});
// Termina Listener de reproductor de audio	
	scrollView.add(contenedor0);
	scrollView.add(contenedor1);
	scrollView.add(contenedor2);
	if(activarFotos==true){
		scrollView.add(contenedorFotos);
	}
	if (urlMusic != '' && urlMusic != null) {
		scrollView.add(repView);
	}
	if (weburl != '' && weburl != null) {
		contenedorVideo.add(youtubeImagen);
		contenedorVideo.add(youtubeVideo);
		scrollView.add(contenedorVideo);
	}
	
	scrollView.add(contenedorDescripcion);
	var contenedorBotonesFinales = Ti.UI.createView({
		top : 0,
		right: 5,
		width: 310,
		backgroundColor :'#bfb7aa',
		height : 120
	});
	var lang = Ti.App.Properties.getString('language');
	if (lang == 'es'){
		var desactivarLabel = Titanium.UI.createLabel({
			backgroundColor: 'transparent',
			top:10,
			text : 'Desactivar punto',
			left : 20
		});
		//Boton para cerrar el dialogo	
		var back = Ti.UI.createButton({
			backgroundImage : 'boton_cerrar.png',
			backgroundFocusedImage: 'images/boton_cerrar_seleccionado.png',
			backgroundSelectedImage: 'images/boton_cerrar_seleccionado.png',
			backgroundColor : 'transparent',
			bottom : 10,
			height : 60,
			width : 140
		});
		back.addEventListener('click', function() {
			if (urlMusic != '') {
				audioPlayer.stop();
				audioPlayer.url = '';
			}
			detailWindow.close();
		});
	} else if (lang == 'en'){
		var desactivarLabel = Titanium.UI.createLabel({
			backgroundColor: 'transparent',
			top:10,
			text : 'Deactive Point',
			left : 20
		});	
		//Boton para cerrar el dialogo	
		var back = Ti.UI.createButton({
			backgroundImage : 'images/Button_Cancel.png',
			backgroundFocusedImage: 'images/Button_Cancel_Selected.png',
			backgroundSelectedImage: 'images/Button_Cancel_Selected.png',
			backgroundColor : 'transparent',
			bottom : 10,
			height : 60,
			width : 110
		});
		back.addEventListener('click', function() {
			if (urlMusic != '') {
				audioPlayer.stop();
				audioPlayer.url = '';
			}
			detailWindow.close();
		});
	}
	
	Ti.API.info(pinActivo.toString());
	var desactivarPunto = Titanium.UI.createSwitch({
		value: pinActivo.toString(),
		right : 20,
		backgroundColor: 'transparent',
		top:10
	});
	
	desactivarPunto.addEventListener('change', function(e) {
		for(j=0;j<routes.length;j++){
			if (routes[j]['posicion'] == id) {
				if (routes[j].status == 'Activado') {
					routes[j].status= 'Desactivado';
					Ti.API.info("ruta modificada: "+routes[j].status);
				} else {
					routes[j].status= 'Activado';
					Ti.API.info("ruta modificada: "+routes[j].status);
				}
			}
		}
		Ti.App.Properties.setString('routes',JSON.stringify(routes));
	});
	
	if (Titanium.App.Properties.getString(Punto) == 'Activado') {
		desactivarPunto.setValue(true);
	} else {
		desactivarPunto.setValue(false);
	}

		 
	contenedorBotonesFinales.add(desactivarLabel);	
	contenedorBotonesFinales.add(desactivarPunto);
	desactivarPunto.value=pinActivo;
	contenedorBotonesFinales.add(back);
	scrollView.add(contenedorBotonesFinales);
	detailWindow.add(scrollView);
	/*detailWindow.addEventListener('open', function() {
		progressBar.show();
	});*/

	return detailWindow;
}

module.exports = DetailPointWindow;
