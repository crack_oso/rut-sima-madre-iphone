var MapWindow = function(){
	//Ti.App.Properties.setString('windowSelected', 'ui/iphone/MapWindow.js');
	Ti.Geolocation.purpose = "Ubicacion actual";
	var http = require('helpers/http');	
	var DetailPointWindow = require('ui/iphone/DetailPointWindow');
	var self = Ti.UI.createWindow({
		zIndex:0,
		title:'Rutisima',
		barColor :'#111',
		bottom:49,
		translucent:true
	});		
	
	var route = null;
	
	
	
	var whereButton = Ti.UI.createButton({
    	title:'Dónde estoy?',    	
    });
	
	var botonTrazaRuta = Titanium.UI.createButtonBar({
		labels:[{title:'Ir', width:30}],
		backgroundColor:'#16a4fa',
	});
	
	var mapView = Titanium.Map.createView({
    	mapType: Titanium.Map.STANDARD_TYPE,
        animate:true,
        regionFit:true,
        userLocation:true,
       	region:{latitude:19.433653, longitude:-99.138222, latitudeDelta:0.1, longitudeDelta:0.1}
    });

	var bar = Ti.UI.createImageView({
		image : '../../images/barSimulation.png',
        width : 320,
        height : 45,
        top:0
	})
	mapView.add(bar);
    var pins = function(points){
    	var pinView = Ti.UI.createImageView({
    		//image : '../../images/pin_mapa_red.png',
        	width : 30,
        	height : 41
    	});
    	var pinImage = Ti.UI.createImageView({
        	image : '../../images/pin_mapa_red.png',
        	width : 30,
        	height : 41
    	});
    	var pinImageDisabled = Ti.UI.createImageView({
        	image : 'images/pin_mapa_gray.png',
        	width : 30,
        	height : 41
    	});
    	
    	var childView = Ti.UI.createImageView({
    		image : 'images/pop_up_on.png',
        	width : 20,
        	height : 20
    	});      	
    	
    	//Ti.API.info(points);
    	for(var i = 0; i< points.length; i++){
    		var anotacion = Titanium.Map.createAnnotation({
				latitude : points[i].latitud,
				longitude : points[i].longitud,
				title : points[i].titulo,
				posicion : points[i].posicion,	
				subtitle: points[i].descripcion,			
				animate : true,
				image : pinImage.toImage(),
				leftView : pinView,
				rightView: childView
			});					
		mapView.addAnnotation(anotacion);			
		}								
    };
    
    
    self.leftNavButton = whereButton;
    self.rightNavButton = botonTrazaRuta;   
    self.add(mapView);
    
    Ti.App.addEventListener('NewPins', function(e){
    	if(route != null){mapView.removeRoute(route);}    	
    	mapView.removeAllAnnotations();    	
    	var routes = e.data;
		pins(routes);
    });
    
    whereButton.addEventListener('click', function(){    	
    	Ti.Geolocation.getCurrentPosition(function(e) {
    		mapView.region = {latitude:e.coords.latitude, longitude:e.coords.longitude, latitudeDelta:0.1, longitudeDelta:0.1}
    	});
    });
	
	botonTrazaRuta.addEventListener('click', function(e){
		if (e.index == 0){
			Ti.Geolocation.getCurrentPosition(function(e) {
    			var points = JSON.parse(Ti.App.Properties.getString('routes'));						
				var routeData = [];
				
				routeData.push({latitude:e.coords.latitude, longitude:e.coords.longitude});
				for(var i = 0; i< points.length; i++){
					routeData.push({latitude:points[i].latitud, longitude:points[i].longitud});	
				}
				for(x=0;x<routeData.length;x++){
					Ti.API.info(routeData[x].latitude + " " + routeData[x].longitude);	
				}												
				if(route != null){
					mapView.removeRoute(route);
				}
				route = {
					name: 'myroute',
					width: 2,
					color:'#9c5aaf',
					points: routeData
				}
				mapView.addRoute(route);
	    	});			
		}
	});
	
	mapView.addEventListener('click', function(e){
		if(e.clicksource == 'title' || e.clicksource == 'subtitle'){			
			if(e.annotation.posicion){
				Ti.API.info(e.annotation.posicion);								
				var detailPointWindow = new DetailPointWindow(e.annotation.posicion, e.annotation.lenght);
				detailPointWindow.open();
			}
		}
	});
	
	self.addEventListener('open', function(){
		if(!Ti.App.Properties.getString('routes')){
			new http.getPoints({
				onload: function(response){
					for(var i =0; i<response.length; i++){						
						response[i].posicion =i+1;
						//Ti.API.info(JSON.stringify(response[i]));						
					}												
					Ti.App.Properties.setString('routes', JSON.stringify(response));					
					pins(response);					
				},
				onerror: function(){
					alert('¿Hay conexión a internet? ');
			}
		});	
		}else{
			var routes = JSON.parse(Ti.App.Properties.getString('routes'));
			pins(routes);	
		}
		
	});
	self.open();
	Ti.App.addEventListener('hideWindow',function(e){
		self.setVisible(false);
	})
	Ti.App.addEventListener('showWindow',function(e){
		self.setVisible(true);
	})	
}

MapWindow();
