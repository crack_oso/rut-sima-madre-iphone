
var windowIdioma = Titanium.UI.currentWindow;
windowIdioma.setOpacity(0.85);
windowIdioma.setBackgroundImage('fondo.png');
windowIdioma.setZIndex(0);

if (Titanium.App.Properties.getString('language') == 'es') {
	var labelIdioma = Titanium.UI.createLabel({
		text : 'Idioma',
		font : {
			fontSize : '20',
			fontWeight : 'bold'
		},
		color : '#fff',
		top : '20dp',
		left : '10dp'
	});
	var english = Titanium.UI.createImageView({
		image : 'english_off.png',
		width : '300dp',
		top : '30dp',
		left : '10dp',
		right : '10dp',
	});
	var espanol = Titanium.UI.createImageView({
		image : 'espanol_on.png',
		width : '300dp',
		top : '83dp',
		left : '10dp',
		right : '10dp',
	});

	windowIdioma.add(espanol);
	windowIdioma.add(english);
}
if (Titanium.App.Properties.getString('language') == 'en') {
	var labelIdioma = Titanium.UI.createLabel({
		text : 'Language',
		font : {
			fontSize : '20',
			fontWeight : 'bold'
		},
		color : '#fff',
		top : '20dp',
		left : '10dp'
	});
	var english = Titanium.UI.createImageView({
		image : 'english_on.png',
		width : '300dp',
		top : '30dp',
		left : '10dp',
		right : '10dp',
	});
	var espanol = Titanium.UI.createImageView({
		image : 'espanol_off.png',
		width : '300dp',
		top : '83dp',
		left : '10dp',
		right : '10dp',
	});

	windowIdioma.add(espanol);
	windowIdioma.add(english);
}

var labelNotificaciones = Titanium.UI.createLabel({
	text : 'Notificaciones',
	font : {
		fontSize : '20',
		fontWeight : 'bold'
	},
	color : '#fff',
	top : '205dp',
	left : '10dp'
});

var notificaciones = Titanium.UI.createSwitch({
	top : '200dp',
	left : '160',
});

notificaciones.addEventListener('change',function(e){
	if(Titanium.App.Properties.getString('Notificaciones')=='Activado'){
		Titanium.App.Properties.setString('Notificaciones','Desactivado');
	}else{
		Titanium.App.Properties.setString('Notificaciones','Activado');
	}
	Ti.API.info('Notificaciones: ' + Titanium.App.Properties.getString('Notificaciones'));
});

if( Titanium.App.Properties.getString('Notificaciones')=='Activado'){
	notificaciones.setValue(true);
}else{
	notificaciones.setValue(false);
}

var imagenNotificaciones = Titanium.UI.createImageView({
	image : 'images/Notificaciones_txt-03.png',
	top : '240dp',
	left: '-20',
	width : '390px',
});

var cancelar = Titanium.UI.createButton({
	backgroundImage: 'images/cancelar.png',
	backgroundFocusedImage: 'images/cancelar_selecccionado.png',
	backgroundSelectedImage: 'images/cancelar_seleccionado.png',
	bottom : 30,
	width : 300,
	height:	45
});

cancelar.addEventListener('click', function(e) {
	Ti.App.fireEvent('foo',{name:'bar'});
	windowIdioma.setVisible(false);
});

cancelar.addEventListener('click', function(e) {
	Ti.App.fireEvent('foo',{name:'bar'});
	windowIdioma.setVisible(false);
});

espanol.addEventListener('click', function(e) {
	Ti.App.fireEvent('espanolTabGroup',{name:'bar'});
	Ti.App.fireEvent('espanolMap',{name:'bar'});
	Ti.App.fireEvent('espanolTable',{name:'bar'});
	Titanium.App.Properties.setString('language', 'es');
	Ti.API.info('Se definió como idioma: ' + Titanium.App.Properties.getString('language'));
	english.setImage('english_off.png');
	espanol.setImage('espanol_on.png');
	imagenNotificaciones.setImage('images/Notificaciones_txt-03.png');
});
english.addEventListener('click', function(e) {
	Ti.App.fireEvent('englishTabGroup',{name:'bar'});
	Ti.App.fireEvent('englishMap',{name:'bar'});
	Ti.App.fireEvent('englishTable',{name:'bar'});
	Titanium.App.Properties.setString('language', 'en');
	Ti.API.info('Se definió como idioma: ' + Titanium.App.Properties.getString('language'));
	english.setImage('english_on.png');
	espanol.setImage('espanol_off.png');
	Ti.App.fireEvent('ajustes',{top: '240', left: '-20', width : '390'});
	//Cambiar las longitudes de la imágen
	//imagenNotificaciones.setImage('images/Notificaciones_txt-03-en.png');
});

windowIdioma.add(labelIdioma);
windowIdioma.add(cancelar);
windowIdioma.add(labelNotificaciones);
windowIdioma.add(notificaciones);
windowIdioma.add(imagenNotificaciones);

Ti.App.addEventListener('showOptionsAjustes',function(e){
	winSel = Ti.App.Properties.getString('windowSelected');
		if(winSel=='ui/iphone/MapWindow2.js'){
			windowIdioma.setVisible(true);
			//windowMapaAjustes.setVisible(true);	
		}else if(winSel=='ui/iphone/TableWindow2.js'){
			windowIdioma.setVisible(true);
			//windowTableAjustes.setVisible(true);		
		}else if(winSel=='ui/iphone/informacion2.js'){
			windowIdioma.setVisible(true);
			//windowInformacionAjustes.setVisible(true);		
		}
})

