var ApplicationTabGroup = require('ui/'+Ti.Platform.osname+'/ApplicationTabGroup');
var LanguageWIndow = require('ui/'+Ti.Platform.osname+'/LanguageWindow');
Ti.App.iOS.registerBackgroundService({url:'ui/iphone/backgroundService.js'});
var app;

if(Ti.App.Properties.getString('language')){
	app = new ApplicationTabGroup();
	app.addEventListener('click',function(e){
		Ti.API.info('boton clickeado: ' + e.index);	
	})
}else{
	app = new LanguageWIndow();
}

app.open();
Ti.API.info('el idioma por default es:'+Ti.App.Properties.getString('language'));

