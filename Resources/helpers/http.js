exports.getPoints = function(o){
	var url = '';
	var http = Ti.Network.createHTTPClient({
		onload: function(e){						
			var response = JSON.parse(this.responseText);
			if(o.onload){ 
				o.onload(response);
			}
		},onerror: function(){					
			if(o.onerror){ 
				o.onerror();
			}
		},timeout:10000 
	});		
	if(Ti.App.Properties.getString('language') == 'es'){
		Ti.API.info('es');
		url = "http://www.publicidadenlinea.com/rutisimamadre/web/json.php?idioma=Es&order=posicion";
	}else{
		Ti.API.info('en');
		url = "http://www.publicidadenlinea.com/rutisimamadre/web/json.php?idioma=In&order=posicion";
	}				
	http.open("GET",url);
	http.send();
}